package pl.sda.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.model.Student;
import pl.sda.service.StudentService;

/**
 * Created by patry on 15.03.2017.
 */
@Controller
public class StudentController {


    @Autowired
    @Qualifier("DBStudentService")
    StudentService studentService;

//    @RequestMapping(value = "/removeStudentDetails/{login:.+}", method = RequestMethod.POST)
    @RequestMapping(value = "/remove/{id}", method = RequestMethod.GET)
    public ModelAndView removeStudentDetail(@PathVariable Long id) {
        studentService.removeStudent(id);
        return new ModelAndView("redirect:/studentsList");
    }

    @RequestMapping(value = "/studentDetails", method = RequestMethod.POST)
    public ModelAndView updateStudentDetail(@ModelAttribute Student student) {
        studentService.updateStudent(student);

        return new ModelAndView("redirect:/studentsList");
    }

    @RequestMapping(value = "/studentDetails/{login:.+}", method = RequestMethod.GET)
    public ModelAndView showStudentDetail(@PathVariable String login) {
        ModelAndView model = new ModelAndView();
        model.addObject("studentsList", studentService.getStudentByLogin(login));
        model.setViewName("studentDetails");

        return model;
    }
 /*   //:.+ po ewentualnej kropce nie obetnie loginu
    @RequestMapping(value = "/studentDetails/{login:.+}", method = RequestMethod.POST)
    public ModelAndView showStudentDetail(@PathVariable String login) {
        ModelAndView model = new ModelAndView();
        model.addObject("studentsList", studentService.getStudentByLogin(login));
        model.setViewName("studentDetails");

        return model;
    }
    */

    @RequestMapping(value = "/studentsList", method = RequestMethod.GET)
    public ModelAndView showStudentsList() {
        ModelAndView model = new ModelAndView();
//        model.addObject("studentsList", new InMemoryStudentDAO().getAllStudents());
        model.addObject("studentsList", studentService.getAllStudents());
//        studentService.getAllStudents();
        model.setViewName("studentsList");

        return model;
    }

    @RequestMapping(value = "/studentForm", method = RequestMethod.GET)
    public ModelAndView showStudentForm() {

        ModelAndView model = new ModelAndView();
        model.addObject("student", new Student());
        model.setViewName("studentForm");

        return model;

    }

    @RequestMapping(value = "/studentForm", method = RequestMethod.POST)
    public ModelAndView saveStudentForm(@ModelAttribute Student student) {
        studentService.addStudent(student);
        return new ModelAndView("redirect:/studentsList");

      /*  model.setViewName("studentForm");

        model.addObject("studentDetails", student.getLogin() + " "
                + student.getFirstName() + " "
                + student.getLastName() + ""
                + student.getIndexNumber());

        studentService.addStudent(student);
        model.addObject("testList", Specialisation.values());
        return model;*/

    }
}
