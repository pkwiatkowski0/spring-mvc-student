package pl.sda.model;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

//w gicie zrobić 2 branche dla studenta i dla book. W masterze póki co zostawić pusty projekt

/**
 * Created by patry on 15.03.2017.
 */
@Entity
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String login;
    private String firstName;
    private String lastName;
    private Integer indexNumber;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Enumerated(EnumType.STRING)
    private Specialisation specialisation;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
//    @Pattern(regexp = "[\\u0061-\\u0079]*", message = "First name has invalid characters")
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
//    @Pattern(regexp = "[\\u0061-\\u0079]*", message = "First name has invalid characters")

    @NotNull(message = "First name is compulsory")
    @NotBlank(message = "First name is compulsory")
    @Pattern(regexp = "[a-z-A-Z]*", message = "First name has invalid characters")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @NotNull(message = "LastName name is compulsory")
    @NotBlank(message = "LastName name is compulsory")
    @Pattern(regexp = "[a-z-A-Z]*", message = "First name has invalid characters")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

//    @NotNull(message = "First name is compulsory")
//    @NotBlank(message = "First name is compulsory")
    public Integer getIndexNumber() {
        return indexNumber;
    }

    public void setIndexNumber(Integer indexNumber) {
        this.indexNumber = indexNumber;
    }

    public Specialisation getSpecialisation() {
        return specialisation;
    }

    public void setSpecialisation(Specialisation specialisation) {
        this.specialisation = specialisation;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

}
