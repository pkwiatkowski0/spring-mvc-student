package pl.sda.model;

/**
 * Created by patry on 15.03.2017.
 */
public enum Gender {
    MALE, FEMALE, OTHER;

}
