package pl.sda.model;

/**
 * Created by patry on 15.03.2017.
 */
public enum Specialisation {
    MED, IT, MGMT, LAW;
}
