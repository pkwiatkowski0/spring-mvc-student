package pl.sda.service;

import pl.sda.model.Student;

import java.util.List;

/**
 * Created by patry on 15.03.2017.
 */
public interface StudentService {

   Student getStudentByLogin(String login);

   void addStudent(Student student);

   void removeStudent(Long id);
//   void removeStudent(Student student);

   void updateStudent(Student student);

   List<Student> getAllStudents();

   Student findStudentByName(String name);
}
