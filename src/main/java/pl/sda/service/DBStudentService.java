package pl.sda.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.model.Student;
import pl.sda.repositories.StudentRepository;

import java.util.List;

/**
 * Created by patry on 18.03.2017.
 */
@Service("DBStudentService")
public class DBStudentService implements StudentService {

    @Autowired
    StudentRepository studentRepository;

  /*@Override
    List<Student> findByFirstName(String firstName) {
        return studentRepository.findByFirstName(firstName);
    }
*/
    @Override
    public Student getStudentByLogin(String login) {

        return studentRepository.findByLogin(login);
    }

    @Override
    public void addStudent(Student student) {
        studentRepository.save(student);
    }

    @Override
    public void removeStudent(Long id) {
        studentRepository.delete(id);
    }


    @Override
    public void updateStudent(Student student) {
        studentRepository.save(student);
    }

    @Override
    public List<Student> getAllStudents() {
        return studentRepository.findAll();
    }

    @Override
    public Student findStudentByName(String name) {
        return findStudentByName(name);
    }


}
