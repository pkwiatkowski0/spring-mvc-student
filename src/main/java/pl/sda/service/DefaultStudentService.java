package pl.sda.service;

import pl.sda.model.Student;
import pl.sda.dao.StudentDao;

import java.util.List;

/**
 * Created by patry on 15.03.2017.
 */
public class DefaultStudentService implements StudentService {


    private StudentDao studentDAO;

    public StudentDao getStudentDAO() {
        return studentDAO;
    }

    public void setStudentDAO(StudentDao studentDAO) {
        this.studentDAO = studentDAO;
    }

    @Override
    public Student getStudentByLogin(String login) {
        return studentDAO.getStudentByLogin(login);
    }

    @Override
    public void addStudent(Student student) {
        studentDAO.addStudent(student);
    }

    @Override
    public void removeStudent(Long id) {
//        studentDAO.removeStudent(id);
    }

    @Override
    public void updateStudent(Student student) {
        studentDAO.updateStudent(student);
    }

    @Override
    public List getAllStudents() {
        return studentDAO.getAllStudents();
    }

    @Override
    public Student findStudentByName(String name) {
        return studentDAO.findStudentByName(name);
    }
}
