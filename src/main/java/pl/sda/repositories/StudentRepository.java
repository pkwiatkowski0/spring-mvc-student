package pl.sda.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.sda.model.Student;

import java.util.List;

/**
 * Created by patryk on 18.03.2017.
 */
@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

    Student findByLogin(String login);

//    @Query("SELECT DISTINCT (s) FROM student s WHERE s.firstName = ?1")
    List<Student> findByFirstName(String firstName);

//    List<Student> findAllByFirstName(String firstName);

//    Student findStudent(String firstName);

}
