package pl.sda.dao;

import pl.sda.model.Student;

import java.util.List;

/**
 * Created by patry on 15.03.2017.
 */
public interface StudentDao {

    Student getStudentByLogin(String login);

    void addStudent(Student student);

    void removeStudent(String login);

    void updateStudent(Student student);

    List getAllStudents();

    Student findStudentByName(String name);


}
