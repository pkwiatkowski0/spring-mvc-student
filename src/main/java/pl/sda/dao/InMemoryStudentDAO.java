package pl.sda.dao;

import pl.sda.model.Student;

import java.util.List;

/**
 * Created by patry on 15.03.2017.
 */
public class InMemoryStudentDAO implements StudentDao {


    private List<Student> studentsList;


    @Override
    public Student getStudentByLogin(String login) {
        return  studentsList.stream().filter(o -> o.getLogin().equals(login)).findAny().get();
    }

    @Override
    public void addStudent(Student student) {
        studentsList.add(student);
    }

    @Override
    public void removeStudent(String login) {
        if (studentsList.contains(login)) {
            studentsList.remove(login);
        }
    }

    @Override
    public void updateStudent(Student student) {
        if (studentsList.contains(student)) {
            studentsList.remove(student);
            studentsList.add(student);
        }
    }

    @Override
    public List getAllStudents() {
        return studentsList;
    }

    @Override
    public Student findStudentByName(String name) {
        return studentsList.stream().filter(o -> o.getFirstName().equals(name)).findAny().get();
    }

    public List<Student> getStudentsList() {
        return studentsList;
    }

    public void setStudentsList(List<Student> studentsList) {
        this.studentsList = studentsList;
    }
}
