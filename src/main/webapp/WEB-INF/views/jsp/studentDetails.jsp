<%--
  Created by IntelliJ IDEA.
  User: patryk
  Date: 16.03.2017
  Time: 20:34
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Student Details</title>
</head>
<body>
<div>
    <%--dodac tutaj formularz w commandnme podac studentList, action na metode ktora robi update studenta--%>
    <label path="firstName">login:</label>
    <br>
    <h1>${studentsList.login} </h1>

    <form:form action="/studentDetails" method="post" commandName="studentsList">
        <form:hidden path="id"/>
        <div>
            <form:label path="login">login:</form:label>
            <br>
            <form:input id="login" path="login" type="text"/>
        </div>
        <div>
            <form:label path="firstName">firstname:</form:label>
            <br>
            <form:input id="firstName" path="firstName" type="text"/>
        </div>
        <div>
            <form:label path="lastName">lastname:</form:label>
            <br>
            <form:input id="lastName" path="lastName" type="text"/>
        </div>
        <div>
            <form:label path="indexNumber">indexNumber:</form:label>
            <br>
            <form:input id="indexNumber" path="indexNumber" type="text"/>
        </div>
        <div>
            <form:label path="gender"> gender</form:label>
            <form:radiobuttons path="gender"></form:radiobuttons>
        </div>
        <div>
            <form:label path="specialisation">specialisation</form:label>
            <form:select path="specialisation">
                <form:options items="${specsList}"/>
            </form:select>
        </div>
        <input type="submit" value="Update">

    </form:form>

    <form:form action="/studentDetails/{id}" method="delete" commandName="studentsList">
        <%--<form:form action="/studentForm" method="post" commandName="student">--%>
        <input type="submit" value="Remove student">
    </form:form>


</div>

</body>
</html>
