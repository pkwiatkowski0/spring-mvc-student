<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: patry
  Date: 15.03.2017
  Time: 19:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Add Student</title>

    <spring:url value="/resources/core/css/hello.css" var="coreCss"/>
    <link href="${coreCss}" rel="stylesheet"/>
    <spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss"/>
    <link href="${bootstrapCss}" rel="stylesheet"/>
    <spring:url value="/resources/core/css/sticky-footer-navbar.css" var="stickyCss"/>
    <link href="${stickyCss}" rel="stylesheet"/>

</head>
<body>
<form:form action="/studentForm" method="post" commandName="student">
    <div>
        <form:label path="login">login:</form:label>
        <br>
        <form:input id="login" path="login" type="text"/>
    </div>
    <div>
        <form:label path="firstName">firstname:</form:label>
        <br>
        <form:input id="firstName" path="firstName" type="text"/>
    </div>
    <div>
        <form:label path="lastName">lastname:</form:label>
        <br>
        <form:input id="lastName" path="lastName" type="text"/>
    </div>
    <div>
        <form:label path="indexNumber">indexNumber:</form:label>
        <br>
        <form:input id="indexNumber" path="indexNumber" type="text"/>
    </div>
    <div>
        <form:label path="gender"> gender</form:label>
        <form:radiobuttons path="gender"></form:radiobuttons>
    </div>
    <div>
        <form:label path="specialisation">specialisation</form:label>
        <form:select path="specialisation">
            <%--<form:options items="${testList}"/>--%>
            <form:options items="${specsList}"/>
        </form:select>
    </div>
    <input type="submit" value="OK">


</form:form>
</body>
</html>
