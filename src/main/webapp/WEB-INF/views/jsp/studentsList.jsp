<%--
  Created by IntelliJ IDEA.
  User: patry
  Date: 16.03.2017
  Time: 19:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<html>
<head>
    <title>Students List</title>
</head>
<body>
<header><table>
    <tr>
        <td>
            <h3><a href="/studentForm"> Add Student</a>
            </h3>
        </td>
    </tr>
</table></header>

<form action="/studentsList" method="get" commandName="student">

<%--form:label path="firstName">Search by name:</form:label>
    <form:input id="firstName" path="firstName" type="text"/>
    <input type="submit" value="Find">
</form>--%>



<table style="width: 50%; border: thick solid black" ; cellpadding="5" ; cellspacing="5">
    <%--<table style="background-color: aquamarine">--%>
    <tr style="width: auto; text-align:center; background-color: coral">
        <td><h1>Login</h1></td>
        <td><h1>FirstName</h1></td>
        <td><h1>LastName</h1></td>
        <td><h1>IndexNumber</h1></td>
        <td><h1>Specialisation</h1></td>
        <td><h1>Gender</h1></td>
    </tr>

    <c:forEach items="${studentsList}" var="item">
        <tr>
            <td style="width: auto; text-align:center; border: 1px solid #000">
                <h3><a href="/studentDetails/${item.login}"> ${item.login}</a>
                </h3>
            </td>
            <td style="width: auto; text-align:center; border: 1px solid #000">
                <h3>${item.firstName}
                </h3>
            </td>
            <td style="width: auto; text-align:center; border: 1px solid #000">
                <h3>${item.lastName}
                </h3>
            </td>
            <td style="width: auto; text-align:center; border: 1px solid #000">
                <h3>${item.indexNumber}
                </h3>
            </td>
            <td style="width: auto; text-align:center; border: 1px solid #000">
                <h3>${item.specialisation}
                </h3>
            </td>
            <td style="width: auto; text-align:center; border: 1px solid #000">
                <h3>${item.gender}
                </h3>
            </td>
            <td style="width: auto; text-align:center; border: 1px solid #000">
                <h3><a href="/remove/${item.id}"> X</a>
                </h3>
            </td>

        </tr>
    </c:forEach>
</table>

</body>
</html>
